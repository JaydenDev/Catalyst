# Please Help!
## Catalyst is under-maintained and cannot receive major updates!
We need more contributors, otherwise we cannot bring the features planned for 2.0 \
If you or someone you know can code with Electron applications and HTML/CSS/JS \
please help. This browser and other software heavily depend on community support \
as its not an organization or any of such. Thank you for using Catalyst
![Catalyst](https://raw.githubusercontent.com/JaydenDev/Catalyst/master/assets/banner.svg)
An amazing and elegant Electron web browser.
![img](https://user-images.githubusercontent.com/92550746/147862353-c30a1246-1ab6-48b0-992f-f138b8f95648.png)
## Stay Secure
With, at least I hope, no vulnerabilities your data will stay secure!
## No Privacy Concerns
ZERO data collection!
## Fast and stable
Starts in seconds, no matter the hardware
## Great UI
Powered by TailwindCSS, this amazing UI is great to look at.
Neat and modern icons by Bootstrap Icons are easy to understand. They are in `assets/icons`.
# OS Version Requirements.
MacOS Min (10.11 El Capitan) Max (12.1 Monterey) \
Windows Min (Windows 7) Max (Windows 11) \
Linux Min (UBU 14* FED 24 DEB 8) Max (UBU 20* FED RAWHIDE DEB 11)
# Thanks to
@webdev03, UI Design/ Feature Design \
@JaydenDev, Repository Maintainer \
@VelocityDesign, Icons \
@MystPi, Original code for tabs 
## If you deserve to be here and aren't, let @JaydenDev know!
